import { Component, OnInit } from '@angular/core';

import { SignalrService } from './library/signalr.service';

@Component({
  selector: 'signalr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'signalR';
  messages = [];

  constructor(public service: SignalrService) { }

  ngOnInit(): void {
    this.service.on('Send', (...args: any[]) => {
      this.messages.push(args[0]);
    });

    this.service.closed.subscribe(error => {
      this.messages.push('disconnected ' + error);
    });
  }

  connect(): void {
    this.service.connect().subscribe(() => {
      this.messages.push('connected');
    }, error => {
      this.messages.push(error);
    });
  }

  disconnect(): void {
    this.service.disconnect();
  }

  brodcast(message: string): void {
    this.service.invoke('Send', message);
  }

  joinGroup(group: string): void {
    this.service.invoke('JoinGroup', group);
  }

  leaveGroup(group: string): void {
    this.service.invoke('Leave', group);
  }

  sendToMe(message: string): void {
    this.service.invoke('Echo', message);
  }

  send(message: string, to: string): void {
    this.service.invoke('Send', message, to);
  }
}
