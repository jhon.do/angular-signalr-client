import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { SignalrModule, HubSettings } from './library';
import { AppComponent } from './app.component';

const hubSettings = {
  url: 'http://localhost:5000/hubs'
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SignalrModule
  ],
  providers: [
    { provide: HubSettings, useValue: hubSettings }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
