import { browser, by, element } from 'protractor';

export class AngularSignalrClientPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('signalR-root h1')).getText();
  }
}
