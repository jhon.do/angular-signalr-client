import { AngularSignalrClientPage } from './app.po';

describe('angular-signalr-client App', () => {
  let page: AngularSignalrClientPage;

  beforeEach(() => {
    page = new AngularSignalrClientPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to signalR!');
  });
});
