# angular-signalR-client

Angular library to connect to an [signalR](https://github.com/aspnet/SignalR) core hub.  
The library depends on *signalr-client*, which is published on [aspnetcore-ci-dev](https://dotnet.myget.org/feed/aspnetcore-ci-dev/package/npm/signalr-client).  
You need to run the following command to install it:  
```bash
npm install signalr-client --registry https://dotnet.myget.org/f/aspnetcore-ci-dev/npm/
```
